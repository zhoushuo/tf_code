#-*- encoding:utf-8 -*-

#author:    zhoushuo
#date:      20180624
#main:      tensorflow basic net

from __future__ import print_function
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

def add_layer(inputs, in_size, out_size, activation_function=None):
    weights = tf.Variable(tf.random_normal([in_size, out_size]))
    biases = tf.Variable(tf.zeros([1, out_size]) + 0.1)
    wx_plus_b = tf.matmul(inputs, weights) + biases
    if activation_function:
        outputs = activation_function(wx_plus_b)
    else:
        outputs = wx_plus_b
    return outputs

def build_sample_data():
    x_data = np.linspace(-1, 1, 300)[:,np.newaxis]
    noise = np.random.normal(0, 0.05, x_data.shape)
    y_data = np.square(x_data) - 0.5 + noise
    return x_data, y_data


def main():
    
    x_data, y_data = build_sample_data()
    
    xs = tf.placeholder(tf.float32, [None, 1])
    ys = tf.placeholder(tf.float32, [None, 1])
    layer1 = add_layer(xs, 1, 10, activation_function=tf.nn.relu)
    prediction = add_layer(layer1, 10, 1)
    
    loss = tf.reduce_mean(tf.reduce_sum(tf.square(ys - prediction), 1))
    #loss = tf.reduce_mean(tf.reduce_sum(tf.square(ys - prediction),
    #    reduction_indices=[1]))
    
    train = tf.train.GradientDescentOptimizer(0.1).minimize(loss)
    
    init = tf.global_variables_initializer()
    sess = tf.Session()
    sess.run(init)

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax.scatter(x_data, y_data)
    #plt.ion()
    plt.show(block=False)
    
    for i in range(1000):
        sess.run(train, feed_dict={xs: x_data, ys: y_data})
        if i % 50 == 0:
            #print(sess.run(loss, feed_dict={xs: x_data, ys: y_data}))
            prediction_value = sess.run(prediction, 
                feed_dict={xs: x_data, ys: y_data})
            lines = ax.plot(x_data, prediction_value, 'r-', lw=5)
            plt.pause(0.5)
            lines[0].remove()


main()
