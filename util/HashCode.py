#coding=utf-8
import sys

def convert_n_bytes(n, b):
    bits = b * 8
    return (n + 2**(bits-1)) % 2**bits - 2**(bits-1)

def convert_4_bytes(n):
    return convert_n_bytes(n, 4)

def getHashCode(s, module=0):
    h = 0
    n = len(s)
    for i, c in enumerate(s):
        h = h + (ord(c) * 31) ** (n-1-i)
    result = convert_4_bytes(h)
    if module:
        return result % module
    else:
        return result



if __name__ == "__main__":
    if len(sys.argv) == 2:
        print getHashCode(sys.argv[1])
    elif len(sys.argv) == 3:
        print getHashCode(sys.argv[1], int(sys.argv[2]))
    else:
        print "Usage: python HashCode.py string [model]"
