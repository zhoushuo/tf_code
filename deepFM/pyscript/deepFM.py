#coding=utf-8

from __future__ import print_function
import numpy as np
import pandas as pd
import tensorflow as tf


def input_file_fn(filename, batch_size=2, num_epochs=1, shuffle=0):
    def decode_file(line):
        cols = tf.string_split([line], "\t")

        label = tf.string_to_number(cols.values[2], out_type=tf.float32)

        t_cont_str = tf.string_split([cols.values[0]], ",")
        t_cate_str = tf.string_split([cols.values[1]], ",")

        cont_features = tf.string_to_number(t_cont_str.values, out_type=tf.float32)
        cont_features = tf.reshape(cont_features, t_cont_str.dense_shape)
        cate_features = tf.ones(t_cate_str.dense_shape, dtype=tf.float32)

        cont_index = tf.range(13, dtype=tf.int32)
        cont_index = tf.reshape(cont_index, t_cont_str.dense_shape)

        cate_index = tf.string_to_number(t_cate_str.values, out_type=tf.int32)
        cate_index = tf.reshape(cate_index, t_cate_str.dense_shape)
        cate_index = cate_index + 13
    
        feat_val = tf.concat([cont_features, cate_features], 1)
        feat_idx = tf.concat([cont_index, cate_index], 1)
        
        return {"feat_val": feat_val, "feat_idx": feat_idx}, label

    dataset = tf.data.TextLineDataset(filename).map(decode_file)

    if shuffle:
        dataset = dataset.shuffle(shuffle)

    dataset = dataset.repeat(num_epochs)
    dataset = dataset.batch(batch_size)
    
    interator = dataset.make_one_shot_iterator()
    batch_features, batch_labels = interator.get_next()

    return batch_features, batch_labels


def deepfm_model_fn(features, labels, mode, params):
    field_size = params["field_size"]
    feature_size = params["feature_size"]
    embedding_size = params["embedding_size"]
    l2_reg = params["l2_reg"]
    layers = map(int, params["deep_layers"].split(","))
    dropout = map(float, params["dropout"].split(",")

    fm_bias = tf.get_variable(name="fm_bias", shape=[1], initializer=tf.constant_initializer(0.0))
    fm_wights = tf.get_variable(name="fm_wights", shape=[feature_size], initializer=tf.glorot_normal_initializer())
    fm_vector = tf.get_variable(name="fm_vector", shape=[feature_size, embedding], initializer=tf.glorot_normal_initializer())

    feat_idx = features["feature_idx"]
    feat_idx = tf.reshape(feat_idx, shape=[-1, field_size])
    feat_val = features["feature_val"]
    feat_val = tf.reshape(feat_val, shape=[-1, field_size])

    #--------------FM component---------------
    with tf.variable_scope("first-order"):
        feat_wgts = tf.nn.embedding_lookup(fm_weights, feat_idx)
        y_weight = tf.reduce_sum(tf.multiply(feat_wgts, feat_val), 1)

    with tf.variable_scope("second-order"):
        embeddings = tf.nn.embedding_lookup(fm_vector, feat_idx)
        feat_val = tf.reshape(feat_val, shape=[-1. field_size, 1])
        embeddings = tf.multiply(embeddings, feat_val)
        sum_square = tf.square(tf.reduce_sum(embedding, 1))
        square_sum = tf.reduce_sum(tf.square(embedding), 1)
        y_vector = 0.3 * tf.reduce_sum(tf.subtract(sum_square, square_sum), 1)

    #--------------deep component------------
    with tf.variable_scope("deep-part"):
        deep_inputs = tf.reshape(embedding, shape=[-1, field_size * embedding_size])
        for idx, output in enumerate(layers):
            deep_inputs = tf.contrib.layers.fully_connected(
                inputs = deep_inputs,
                num_outputs = output,
                weights_regularizer = tf.contrib.layers.l2_regularizer(l2_reg),
                scope = "bn_%s" %i)
            if mode == tf.estimator.ModeKeys.TRAIN:
                deep_inputs = tf.nn.dropout(deep_inputs, keep_prob=dropout[i])

        y_deep = tf.contrib.layers.fully_connected(
            inputs = deep_inputs,
            num_outputs = 1,
            activation_fn = tf.identity,
            weights_regularizer=tf.contrib.layers.l2_regularizer(l2_reg),
            scope = "deep_out")
        y_deep = tf.reshape(y_deep, shape=[-1])

    
    #-------------deepfm---------------
    with tf.variable_scope("deepfm-out"):
        y_bias = fm_bias * tf.ones_like(y_deep, dtype=tf.float32)
        y = y_bias + y_weight + y_vector + y_deep
        pred = tf.sigmoid(y)


        



def main():
    batch_features, batch_labels = input_file_fn("../data/train")
    sess = tf.Session()
    value = sess.run(batch_features)
    label = sess.run(batch_labels)
    print(label)
    print(value["feat_idx"].reshape((-1, 39)))
    print(value["feat_val"].reshape((-1, 39)))
    


if __name__ == "__main__":
    main()
